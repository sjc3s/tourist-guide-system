<?php
function getTags()
{
    include("connection.php");
    $result =  $conn->query("SHOW COLUMNS FROM tags");
    if (!$result) {
        echo 'Could not run query: ' . mysql_error();
        exit;
    }
    $i=0;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $a[$i]=$row['Field'];
            $i++;
        }
    }
    return implode(",",$a);
}
?>