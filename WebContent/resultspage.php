<?php
include('session.php');
?>

<!DOCTYPE html>
<html>
<head>
<style>

div 
{
  font-family: Verdana, Arial, Helvetica, sans-serif;
  }



.interest
{
	float: right;
    height: 100vh;
}


 .search {
    width: 300px;
    height:40px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

  .search:focus {
    width: 80%;
}


a.settings {
	float: right;
	}
	*, *::before, *::after {
  box-sizing: border-box;
}


/*Checkboxes styles*/
input[type="checkbox"] { display: none; }

input[type="checkbox"] + label {
  display: block;
  position: relative;
  padding-left: 20%;
  margin-bottom: 3%;
  font: 14px/20px 'Open Sans', Arial, sans-serif;
  color: #6d6d6d;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

input[type="checkbox"] + label:last-child { margin-bottom: 0; }

input[type="checkbox"] + label:before {
  content: '';
  display: block;
  width: 20px;
  height: 20px;
  border: 1px solid #2460e2;
  position: absolute;
  left: 0;
  top: 0;
  opacity: .6;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

input[type="checkbox"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}
/*Radio button styles*/
input[type="radio"] { display: none; }

input[type="radio"] + label {
  display: block;
  position: relative;
  padding-left: 35px;

  font: 14px/20px 'Open Sans', Arial, sans-serif;
  color: #6d6d6d;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

input[type="radio"] + label:last-child { margin-bottom: 0; }

input[type="radio"] + label:before {
  content: '';
  display: block;
  width: 18px;
  height: 18px;
  border: 1px solid #2460e2;
  position: absolute;
  left: 0;
  top: 0;
  opacity: .6;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

input[type="radio"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}
.range {
  -webkit-appearance: none;
  -moz-appearance: none;
  position: relative;
  left: 50%;
  top: 50%;
  width: 130px;
  margin-top: 10px;
  transform: translate(-50%, -50%);
}

input[type=range]::-webkit-slider-runnable-track {
  -webkit-appearance: none;
  background: rgba(59,173,227,1);
  background: -moz-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(59,173,227,1)), color-stop(25%, rgba(87,111,230,1)), color-stop(51%, rgba(152,68,183,1)), color-stop(100%, rgba(255,53,127,1)));
  background: -webkit-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: -o-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: -ms-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3bade3 ', endColorstr='#ff357f ', GradientType=1 );
  height: 2px;
}

input[type=range]:focus {
  outline: none;
}

input[type=range]::-moz-range-track {
  -moz-appearance: none;
  background: rgba(59,173,227,1);
  background: -moz-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(59,173,227,1)), color-stop(25%, rgba(87,111,230,1)), color-stop(51%, rgba(152,68,183,1)), color-stop(100%, rgba(255,53,127,1)));
  background: -webkit-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: -o-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: -ms-linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  background: linear-gradient(45deg, rgba(59,173,227,1) 0%, rgba(87,111,230,1) 25%, rgba(152,68,183,1) 51%, rgba(255,53,127,1) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3bade3 ', endColorstr='#ff357f ', GradientType=1 );
  height: 2px;
}

input[type=range]::-webkit-slider-thumb {
  -webkit-appearance: none;
  border: 2px solid;
  border-radius: 50%;
  height: 25px;
  width: 25px;
  max-width: 80px;
  position: relative;
  bottom: 11px;
  background-color: #1d1c25;
  cursor: -webkit-grab;

  -webkit-transition: border 1000ms ease;
  transition: border 1000ms ease;
}

input[type=range]::-moz-range-thumb {
  -moz-appearance: none;
  border: 2px solid;
  border-radius: 50%;
  height: 25px;
  width: 25px;
  max-width: 80px;
  position: relative;
  bottom: 11px;
  background-color: #1d1c25;
  cursor: -moz-grab;
  -moz-transition: border 1000ms ease;
  transition: border 1000ms ease;
}


.range.blue::-webkit-slider-thumb {
   border-color: rgb(59,173,227);
}

.range.ltpurple::-webkit-slider-thumb {
   border-color: rgb(87,111,230);
}

.range.purple::-webkit-slider-thumb {
   border-color: rgb(152,68,183);
}

.range.pink::-webkit-slider-thumb {
   border-color: rgb(255,53,127);
}

.range.blue::-moz-range-thumb {
   border-color: rgb(59,173,227);
}

.range.ltpurple::-moz-range-thumb {
   border-color: rgb(87,111,230);
}

.range.purple::-moz-range-thumb {
   border-color: rgb(152,68,183);
}

.range.pink::-moz-range-thumb {
   border-color: rgb(255,53,127);
}

input[type=range]::-webkit-slider-thumb:active {
  cursor: -webkit-grabbing;
}

input[type=range]::-moz-range-thumb:active {
  cursor: -moz-grabbing;
}

 }

p.parab{
    color:black;
    text-align:center;

}

.tainer12{
    height:135px;
    width:100%;
    padding: 2%;
    margin-bottom:2%;
    background-color:#e6e6e6;
    border-radius: 5px;
    z-index: -1; /* hide shadow behind image */
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.3);

}
.tainer12:hover{
    background-color:#d9d9d9;
}
.img12{
    max-height:100px;
    max-width:20%;
    padding-right:2%;
	float:left;
	display:inline-block;
}
.tagImg{
    height:100px;
    width:150px;
}


p.recc{
    color:#6d6d6d;
}

.recc, .hisrecc{
    
    padding: 5%;
    height: 110px;
    font-size: 16px;
    background-color: #d8d8d8;
    line-height: 20px;
    margin-bottom:4%;
    border-radius:3px;

}
.recc img, .hisrecc img {
    float: left;    
    margin-right:5%;
    height: 90px;
    width: 90px;
   
}
.recc p, .hisrecc p{
    margin-top:auto;


}
center{
    width:56%;
    margin-left:20%;
    margin-right:25%;
    margin-top:1%;
    background-color:#d8d8d8;
    padding:2.5%;
    height: 89vh;
    z-index: -1;
    -webkit-box-shadow:  0px 2px 8px 1px rgba(0, 0, 0, .7);

}
.filter {
    width:14%;
    height: 89vh;
    background: #d8d8d8;
    float: left;
    overflow: auto;
    margin-top: 1%;
    margin-left: 5%;
    
    padding: 1%;
    z-index: -1;
    -webkit-box-shadow:  0px 2px 8px 1px rgba(0, 0, 0, .7);
}

.group{
    width:18%;
    height: 89vh;
    top:-50px;
    background: #d8d8d8;
    float: right;
    overflow: auto;
    margin-top: 1%;
    margin-right: 5%;
    padding:0.5%;
    z-index: -1;
    -webkit-box-shadow:  0px 2px 8px 1px rgba(0, 0, 0, .7);
    
}
.cont1{
	height: 90%;
	width:100%;
	overflow-y:auto;
	margin-left:auto;
	margin-right:auto;
	margin-top: 3%;
	margin-bottom: 3%;
}
.results
{
	float:left;
	position:relative;
    text-align: left;  
    margin:auto;
	width:100%;
	height:100%;
	padding:3%;
	display:inline-block;
	
}
.recc:hover img{
	opacity: 1;
	-webkit-animation: flash 1.5s;
	animation: flash 1.5s;
}
@-webkit-keyframes flash {
	0% {
		opacity: .4;
	}
	100% {
		opacity: 1;
	}
}
@keyframes flash {
	0% {
		opacity: .4;
	}
	100% {
		opacity: 1;
	}
}

.wrapper1{
	width:auto;
	margin: 0 auto;
}
#p2{
	color:white;
}
body{
    background-color:#f9f9f9;
}
label{
    color:#6d6d6d;
}

</style>
</head>
<link rel="stylesheet" type="text/css" href="main.css">
<body>

<form id= 'searchForm'>
<div class="wrapper1">
 
	<div align = "right" class="container" style="background-color:#777">
		<input type="button" class="button" value="change settings" onclick="location='usersettings.php'">
		<input type="button" class="button" value="logout" onclick="location='logout.php'">
	</div>
	
    <div class="group">
    	<div class="intRecc">
     		<label>Recommendations Based on Interests</label>
     		<hr>
     		<div id = "inter">
     			<!-- each div inside this has the class "recc" -->
    
    	</div>
    </div>
    <div class = "hisRecc">
    <label>Recommendations Based on Search History</label>
    <hr>
    	<div id = "history">
    		<!-- each div inside this has the class "recc" -->
    	</div>
    </div>
    </div>

	<div id="filter" align="left" class = "filter">
		<label >Filters</label>
		<hr>
		<div id="tagCheckBox">
			<?php 
    			include("tools.php");
    			
    			$arr= explode(",",getTags());
    			for($i=0; $i<count($arr);$i++)
    			{
    			    if($arr[$i]!="TID")
    			        echo "<input type=\"checkbox\" id=\"box".$i."\" class=\"round\" onclick=\"searchRes()\" tag=\"".$arr[$i]."\"><label for=\"box".$i."\">".$arr[$i]."</label>";
    			        
    			}
			?>
		</div>
		<hr>
		<div id = "radiobutton">
			<label>Season</label> <br>
        	<input id= "Summer" type = "radio" value = "Summer" class="rb" onclick="selectSeason(this.value)"><label for="Summer">Summer</label>
			 <div class="check"></div>
			 
			 
        	<input id= "Winter" type = "radio"value = "Winter" class="rb" onclick="selectSeason(this.value)"><label for="Winter">Winter</label>
			 <div class="check"></div>
			 
			 
    		<input id= "Monsoon" type = "radio"value = "Monsoon" class="rb" onclick="selectSeason(this.value)"><label for="Monsoon">Monsoon</label>
			 <div class="check"></div>
			 
			 
		</div>
		<div id="tempSlider">
			<label>Min</label>
			<input id = "barMin" type="range" class="range blue" min="0" max="23" value="0" step="1"  onchange="setMin(this.value)" />
			<br>
			<span id="rMin">0</span>
			<br>
			<label>Max</label>
			<input id= "barMax" type="range" class="range blue" min="24" max="45" value="0" step="1" onchange="setMax(this.value)" />
			<br>
			<span id="rMax">24</span>
			
		</div>

		
	</div>
	
	<center>
		
		<input type="text" class="search" name="search" id="searchbar" placeholder="Search.." onkeyup="sendText(this.value)">
    	<div class="cont1">
     		<div id="results" class = "results"></div>
    	</div>
		
    	
	</center>
	
	
	</div>

</form>
<script type="text/javascript" src = "search.js"></script>
<script type="text/javascript" src = "getReccTags.js"></script>
<script type="text/javascript" src = "getReccHistory.js"></script>
</body>
</html>
