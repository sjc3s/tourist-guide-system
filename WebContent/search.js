var searchTerm="";
var season="";
var tempMin= 15, tempMax=45;
function searchRes()
{
	var div = document.getElementById('results');
	while(div.firstChild){
	    div.removeChild(div.firstChild);
	}
	var arr = findActiveTags();
	var jsonObj= "{ \"key\": \""+searchTerm+"\",\"season\": \""+season+"\",\"tempMin\": \""+tempMin+"\",\"tempMax\": \""+tempMax+"\", \"aTags\":[";
	if(arr!=null)
	{
		for(var i =0; i<arr.length;i++)
		{
			jsonObj+="\""+arr[i]+"\"";
			if(i!=arr.length-1)
				jsonObj+=",";
		}
	}
	
	jsonObj+="]}";
	console.log(jsonObj);
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
        	var div = document.getElementById('results');
        	while(div.firstChild){
        	    div.removeChild(div.firstChild);
        	}
        	var text =this.responseText;
            if(text == "No Search Results")
	    	{
            	var para = document.createElement("p");
        	    var node = document.createTextNode("No Search Results");
        	  	para.appendChild(node);
        	  	div.appendChild(para);
	    	}
            else
	    	{
            	var obj = JSON.parse(text); 
                for (var key in obj) {
                	  if (obj.hasOwnProperty(key) ) {
    	            	    var val = JSON.parse(obj[key]);
    	            	    if(val.pid != null){
    	            	    	var div2 = document.createElement("div");
    	                	    var div1 = document.createElement("div");
    	                	    var img = document.createElement("img");
    	                	    var para = document.createElement("p");
    	                	    var para2 = document.createElement("p");
    	                	    var node = document.createTextNode(val.name);
    	                	    var desc = document.createTextNode(val.desc.substring(0,130)+"...");
    	                	    var str = val.img+".jpg";
    	                	    div2.setAttribute("class","tainer12");
    	                	    para2.appendChild(desc);
    	                	    div1.setAttribute("pid",val.pid);
								div1.setAttribute("class","rght");
    	                	    img.setAttribute("src", str);
    	                	    img.setAttribute("height", "100");
    	                	    img.setAttribute("width", "200");
								img.setAttribute("class","img12");
    	                	    
								para.setAttribute("class","parab");
    	                	    
    	                	    div1.appendChild(img);
    	                	    para.appendChild(node);
    	                	    
    	                	    div1.setAttribute("id", "searchResults");
    	                	    div1.setAttribute("onClick", "setClick("+val.pid+")");
    							div1.appendChild(para);
    							div1.appendChild(para2);
    							div2.appendChild(div1);
    	                	    div.appendChild(div2);
    	                	    
    	            	    }
    	            	    
                	  }
                	  
                } 
	    	}
            
            
        }
    };
    xmlhttp.open("GET", "gethint.php?q=" + jsonObj, true);
    xmlhttp.send();
}
function sendText(s)
{
	searchTerm=s;
	searchRes();
}

function findActiveTags()
{
	var div = document.getElementById("tagCheckBox");
	if(div!=null)
	{
		var arr = [];
		var label = div.children;
		var j=0;
		for(var i=0; i<label.length;i++)
		{
			
			if(label[i].nodeName == "INPUT" && label[i].checked==true)
			{
				arr[j++]=label[i].getAttribute("tag");
			}
			
				
		}
		return arr;
	}
	
	
}

function selectSeason(str)
{
	var sum=document.getElementById("Summer");
	var win=document.getElementById("Winter");
	var mon=document.getElementById("Monsoon");
	
	if(str=="Summer")
	{
		win.checked=false;
		mon.checked=false;
		season=str;
	}
	if(str=="Winter")
	{
		sum.checked=false;
		mon.checked=false;
		season= str;
	}
	if(str=="Monsoon")
	{
		win.checked=false;
		sum.checked=false;
		season=str;
	}
	searchRes();
}
function setMin(newValue)
{
	document.getElementById("rMin").innerHTML=newValue;
	tempMin = newValue;
	searchRes();
}
function setMax(newValue)
{
	document.getElementById("rMax").innerHTML=newValue;
	tempMax = newValue;
	searchRes();
}
function setClick(v)
{
	window.open("placeDesc.php?val="+v);
}
