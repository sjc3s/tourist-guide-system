function getRoute() {
	if (navigator.geolocation) { 
   
	navigator.geolocation.getCurrentPosition(function (position) {  
     
		var latitude = position.coords.latitude;             
		var longitude = position.coords.longitude;                
		var coords = new google.maps.LatLng(latitude, longitude); 
		var directionsService = new google.maps.DirectionsService();
		var directionsDisplay = new google.maps.DirectionsRenderer();
		var mapOptions = 
			{		
				zoom: 15,  
				center: coords, 
				mapTypeControl: true, 
				navigationControlOptions:
				{
					style: google.maps.NavigationControlStyle.SMALL 
				},
				mapTypeId: google.maps.MapTypeId.ROADMAP 
			};
		
		
		map = new google.maps.Map(document.getElementById("map"), mapOptions);
		directionsDisplay.setMap(map);
		directionsDisplay.setPanel(document.getElementById('panel'));
		var request = {
				origin: coords,
				destination: document.getElementById('placeName').textContent,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				var route = response.routes[0];
				var summaryPanel = document.getElementById('directions-panel');
				summaryPanel.innerHTML = '';
				for (var i = 0; i < route.legs.length; i++) {
					var routeSegment = i + 1;
					summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
					'</b><br>';
					summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
					summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
					summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
				}
			}
			else {
				window.alert('Directions request failed due to ' + status);
			}
		});
	});
}

}
