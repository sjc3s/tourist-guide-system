<?php 
    include("connection.php");
    $obj = json_decode($_REQUEST['q']);
    $array = array();
    $arr=array();
    $arrayName=array();
    $arraySeason = array();
    $arrayTag = array();
    $key= $obj->key;
    $activeTags=$obj->aTags;
    $season = $obj->season;
    $tempMin = $obj->tempMin;
    $tempMax = $obj->tempMax;
    $obj= new stdClass();
    $fin_json= new stdClass();
    $sea;
    $sql="";
    if($key!="")
    {
        $sqlState= "SELECT name FROM state";
        $result = $conn->query($sqlState);
        $i=0;
        $hint = "";
        
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $stateName[$i]=$row['name'];
                $i++;
            }
        }
        if ($key !== "") {
            $key = strtolower($key);
            $len=strlen($key);
            foreach($stateName as $name ) {
                if (stristr($key, substr($name, 0, $len))) {
                    if ($hint === "") {
                        $hint = $name;
                    } else {
                        $hint .= ",$name";
                    }
                }
            }
            
        }
        $array = explode(",", $hint);
        $i=0;
        foreach($array as $name)
        {
            $sqlState= "SELECT * FROM state WHERE name='$name'";
            $result = $conn->query($sqlState);
            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    $arrayName[$i]=$row['PID'];
                    $i++;
                }
            } 
        }
    }
    if($season !="")
    {
        $i=0;
        $s = strtolower($season);
        $sqlState= "SELECT * FROM $s WHERE Min> $tempMin and Max<$tempMax";
        $result = $conn->query($sqlState);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                if($s == "summer")
                {
                    $arraySeason[$i]=$row['SID'];
                    $sea = "summer_SID";
                }
                    
                if($s == "winter")
                {
                    $arraySeason[$i]=$row['WID'];
                    $sea = "winter_WID";
                }
                    
                if($s == "monsoon")
                {
                    $arraySeason[$i]=$row['MID'];
                    $sea= "monsoon_MID";
                }
                    
                $i++;
            }
        } 
    }
    $str;
    if(count($activeTags)>0)
    {
        $sqlState= "SELECT * FROM tags WHERE ";
        foreach($activeTags as $tag)
        {
            
            $sqlState.=$tag."='1'";
            if(next($activeTags))
                $sqlState.=" OR ";
        }
        $sqlState.=";";
        $str="";
        $i=0;
        $arr;
        $result = $conn->query($sqlState);
        while($row = $result->fetch_assoc())
        {
            $arrayTag[$i]=$row['TID'];
            $i++;
        }
        
    }
    $i=0;
    if(count($arrayName)>0 or count($arrayTag)>0 or count($arraySeason)>0)
    {
        $sql= "SELECT * FROM state where";
        if(count($arrayName)>0)
        {
            $sql.="(";
            foreach($arrayName as $tag)
            {
                
                $sql.="pid= '".$tag."'";
                if(next($arrayName))
                    $sql.=" OR ";
            }
            $sql.=")";
        }
        if(count($arrayTag)>0)
        {
            if(count($arrayName)>0)
                $sql.=" and ";
            $sql.="(";
            foreach($arrayTag as $tag)
            {
                
                $sql.="Tags_tid= '".$tag."'";
                if(next($arrayTag))
                    $sql.=" OR ";
            }
            $sql.=")";
        }
        if(count($arraySeason)>0)
        {
            if(count($arrayName)>0 or count($arrayTag)>0)
                $sql.=" and ";
            $sql.="(";
            foreach($arraySeason as $tag)
            {
                
                $sql.=$sea."= '".$tag."'";
                if(next($arraySeason))
                    $sql.=" OR ";
            }
            $sql.=")";
        }
        $sql.=";";
    }
    if(strlen($sql)==0)
        echo "No Search Results";
    else
    {
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc())
        {
            $name = $row['Name'];
            $obj->pid=$row['PID'];
            $obj->name=$row['Name'];
            $obj->bttv=$row['BTTV'];
            $obj->cost=$row['Cost'];
            $obj->category=$row['Category'];
            $obj->tags=$row['Tags_TID'];
            $obj->mid=$row['Monsoon_MID'];
            $obj->sid=$row['Summer_SID'];
            $obj->wid=$row['Winter_WID'];
            $obj->img=$row['Img_src'];
            $obj->desc=$row['Description'];
            $fin_json->$name=json_encode($obj);
        }
        echo json_encode($fin_json);
    }
    
?>