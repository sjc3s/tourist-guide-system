<html>
	<head>
		<style>
		.imgNotSelected {
		    padding: 10px;
			height: 220px;
			margin:10px;
			width: 200px;
			color:black;
			display: inline-block;
			}
	.imgNotSelected img{
   -webkit-transform: scale(1);
	transform: scale(1);
	-webkit-transition: .3s ease-in-out;
	transition: .3s ease-in-out;}

		
        p{
			font-size:20;
			margin:0;
			
		}
		/* Add a hover effect (blue shadow) */
		.imgNotSelected:hover img{
			box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
		}
		.imgNotSelected:hover img{
			-webkit-transform: scale(1.3);
		transform: scale(1.3);}
		.imgClick{
			
			padding: 10px;
			height: 220px;
			margin:10px;
			width: 200px;
			color:black;
			display: inline-block;
			
		}
		
 .row {
  width: 100%;
  text-align: center; // center the content of the container
}
 .imgZ{
  
  float: left;
  height: 140px;
  width: 150px;
  margin: 30px;
  overflow: hidden;
 }
  .imgClick img{
	  -webkit-transform: scale(1.3);
		transform: scale(1.3);
		box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
		
		}



	</style>
	</head>
    <body>
	<div class = "container" style="background-color:#777">
	<div align = "right" class="register" style="background-color:#777">
	<input type ="button" class ="button" value ="skip" onclick="location='resultspage.php'"> 
	</div>
	
	</div>
	<link rel="stylesheet" type="text/css" href="main.css">
		<div id = "showAllTags" class="row" align="center">
		
		</div>
		<form>
		<div align="center">
			<button type="button" class="button" onclick="save()">Save</button>
			</div>
		</form>
    <script type="text/javascript">
    var arr=[];
    var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
        	var s = JSON.parse(this.responseText);
        	var div1 = document.getElementById("showAllTags");
        	for(var key in s)
            {
        		if(s[key]!="TID")
        		{
            		var div = document.createElement("div");
            		div.setAttribute("onclick","changeStyle(\""+s[key]+"\")");
            		div.setAttribute("class","imgNotSelected");
            		div.setAttribute("id",s[key]);
            		var img = document.createElement("img");
            		img.setAttribute("src",s[key]+".jpg");
					img.setAttribute("class","imgZ");
            		var txt = document.createTextNode(s[key]);
					var p = document.createElement("p");
					p.appendChild(txt);
            		div.appendChild(img);
            		div.appendChild(p);
            		div1.appendChild(div);
        		}
            }
        }
        
    };
    xmlhttp.open("GET", "getTags.php?", true);
    xmlhttp.send();
    
    function changeStyle(s)
	{
		var a = document.getElementById(s);
		if(a.getAttribute("class")== "imgNotSelected")
			a.setAttribute("class","imgClick");
		else
			a.setAttribute("class","imgNotSelected");
	}
	function save()
	{
		var arr= [];
		var div = document.getElementById("showAllTags");
		var divs = div.children;
		for(var i = 0; i < divs.length; i++)
		{
			if(divs[i].getAttribute("class")== "imgClick")
				arr.push(divs[i].getAttribute("id"));
		}
		if(arr.length!=5)
		{
			alert("Please select 5 interests");
		}
		else
		{
			window.open("saveInterests.php?val="+arr, "_self");
		}
		
	}
    </script>
    </body>
    
</html>