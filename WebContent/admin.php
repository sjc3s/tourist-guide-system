<?php

include 'session.php';
include 'connection.php';
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="main.css">
<body>
<form action="/tgs/WebContent/adminadd.php" method="post" enctype="multipart/form-data">
<style>
input[type="checkbox"] { display: none; }

input[type="checkbox"] + label {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 20px;
  font: 14px/20px 'Open Sans', Arial, sans-serif;
  color: #ddd;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}

input[type="checkbox"] + label:last-child { margin-bottom: 0; }

input[type="checkbox"] + label:before {
  content: '';
  display: block;
  width: 20px;
  height: 20px;
  border: 1px solid #6cc0e5;
  position: absolute;
  left: 0;
  top: 0;
  opacity: .6;
  -webkit-transition: all .12s, border-color .08s;
  transition: all .12s, border-color .08s;
}

input[type="checkbox"]:checked + label:before {
  width: 10px;
  top: -5px;
  left: 5px;
  border-radius: 0;
  opacity: 1;
  border-top-color: transparent;
  border-left-color: transparent;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
}
#reb1{
	color:black;
}


</style>

<div align = "right" class="container" style="background-color:#777">
<input type ="button" class ="button" value ="logout" onclick="location='logout.php'"> 
<input type ="button" class ="button" value ="use the website" style="float:left" onclick="location='resultspage.php'">
</div>
<center>
<table>
<tr>
<td><label>place name: </label></td> <td> <input type="text" id="name" name="placename" class="admin" placeholder="Enter the name of the place " ></input></td></tr>
<tr>
<td><label>Place description:</label></td><td> <textarea id="desc" name="description" class="admin" rows="10" cols="100" placeholder="Enter some description"></textarea></td></tr>
<tr>
<td><label>Best time to visit: </label></td> <td> <input type="text" id="besttime" name="bttv" class="admin" placeholder="Best time to visit"></input></td></tr>
<tr>
<td><label>Cost: </label></td> <td> <input type="text" id="cst" name="cost" class="admin" placeholder="Cost"></input></td></tr>
<tr>
<td><label>Category:</label></td>

<td><div id="tagCheckBox">
			<?php 
    			include("tools.php");
    			$arr= explode(",",getTags());
    			for($i=0; $i<count($arr);$i++)
    			{
    			    if($arr[$i]!="TID")
    			        echo "<input type=\"checkbox\" id=\"box".$i."\" value=\"".$arr[$i]."\" class=\"round\" name=\"category[]\" tag=\"".$arr[$i]."\"><label for=\"box".$i."\">".$arr[$i]."</label>";
    			}
			?>
</div></td></tr>
<tr>
<td><label>Summer</label></td>
<td><input type="text" id="Summer" name="smin" placeholder="min"> <input type="text" id="Summer" name="savg" placeholder="avg"> <input type="text" id="Summer" name="smax" placeholder="max"></td></tr>
<tr>
   <td><label>Winter</label></td>
	<td><input type="text" id="Winter" name="wmin" placeholder="min"> <input type="text" id="Winter" name="wavg" placeholder="avg"> <input type="text" id="Winter" name="wmax" placeholder="max"></td></tr>
	<tr>
<td><label>Monsoon</label></td>
<td><input type="text" id="Monsoon" name="mmin" placeholder="min"> <input type="text" id="Monsoon" name="mavg" placeholder="avg"> <input type="text" id="Monsoon" name="mmax" placeholder="max"></td></tr>
    </table>
    <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*"/>
    <input type="submit" class="button" value="Submit">
    <input type="reset" class="button" value="Reset">
	</center>
</form>
</body>
</html>