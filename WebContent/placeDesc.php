<?php
//include('session.php');

include('connection.php');
session_start();

//making guest session so that it does not get redirected to index.html

if(!isset($_SESSION['login_user'])){
	$_SESSION['login_user'] = "guest";
}
else {
	
	$user_check = $_SESSION['login_user'];
	
	$ses_sql = mysqli_query($conn,"SELECT username FROM user WHERE username = '$user_check' ");
	
	$row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
	
	$login_session = $row['username'];
}

//echo $_SESSION['login_user'];

$pid = $_GET['val'];

$sqlState= "SELECT * FROM state WHERE pid = '$pid'";
$result = $conn->query($sqlState);
$row = $result->fetch_assoc();
$tid = $row['Tags_TID'];
$sid = $row['Summer_SID'];
$wid = $row['Winter_WID'];
$mid = $row['Monsoon_MID'];
$sqlState= "SELECT * FROM tags WHERE tid = '$tid'";
$result = $conn->query($sqlState);
$rowTag = $result->fetch_assoc();
$sqlState= "SELECT * FROM summer WHERE sid = '$sid'";
$result = $conn->query($sqlState);
$rowSum = $result->fetch_assoc();
$sqlState= "SELECT * FROM winter WHERE wid = '$wid'";
$result = $conn->query($sqlState);
$rowWin = $result->fetch_assoc();
$sqlState= "SELECT * FROM monsoon WHERE mid = '$mid'";
$result = $conn->query($sqlState);
$rowMon = $result->fetch_assoc();
for($i = 1; $i<5;$i++)
{
    if($_SESSION['s'.$i]=='null')
    {
        $_SESSION['s'.$i]=$pid;
    }

}

for($i = 1; $i<5;$i++)
{
    $_SESSION['s'.$i]=$_SESSION['s'.($i+1)];
}
$_SESSION['s5']=$pid;
?>

<html>
	<head>
	<link rel="stylesheet" type="text/css" href="testCSS.css">
		<link rel="stylesheet" type="text/css" href="main.css">
		<style type = "text/css">
		  .tagImg{
                height:100px;
                width:150px;
            }
      
            h1{
				color:white;
			}
			p1{
				color:white;
			}
			#mainDiv{
				overflow:auto;
			}
			div.forRegistered{
				display: none;
			}
	.recc{
 
    padding: 0 25px;
    height: 130px;
    font-size: 16px;
	border-color:#777;
	border-style:solid;
	float:left;
	display:inline-block;
    line-height: 20px;

    background-color: #484848;
}
.recc img {
    float: left;
    margin:0 10px 0 -25px;
    height: 90px;
    width: 90px;
   
}
#group1{
	 width:100vw;
	 height: 22vh;
	 top:0px;
     background: #484848;
	 position:relative;
	 bottom:0;
	 overflow-y: hidden;
	 overflow-x:auto;
}
.wrapper1{
	width:auto;
	margin: 0 auto;
	position:relative;
}
.recc:hover img{
	opacity: 1;
	-webkit-animation: flash 1.5s;
	animation: flash 1.5s;
}
@-webkit-keyframes flash {
	0% {
		opacity: .4;
	}
	100% {
		opacity: 1;
	}
}
@keyframes flash {
	0% {
		opacity: .4;
	}
	100% {
		opacity: 1;
	}
}

#directions-panel {
  margin-top: 10px;
  background-color: #FFEE77;
  padding: 10px;
  overflow: scroll;
  height: 174px;
  position:relative;
}
	
		</style>
	</head>
	

	
	<body>
	<div class="wrapper1">
	<div align = "right" class="container" style="background-color:#777">
		
		<input type ="button" class ="button" value ="search" onclick="location='resultspage.php'"> 
		<div class="forRegistered">
			<input type="button" class="button" value="change settings" onclick="location='usersettings.php'">
			<input type="button" class="button" value="logout" onclick="location='logout.php'">
		</div>
	</div>
		<div id="mainDiv">
    		<div id="descDiv">
        		<div id = "left">
        			<img align="center" src = "<?php echo htmlspecialchars($row['Img_src'].".jpg");?>" width= 100%, height = 100% padding= 20></img>
        		</div>
        		<div id = "middle">
        			<h1 align = "center" id="placeName" ><?php echo htmlspecialchars($row['Name']);?></h1>
        			<div id="desc">
        				<p1 id=placeDesc><?php echo htmlspecialchars($row['Description']);?></p1>
        			</div>
        			<p1>Best Time To Visit: <?php echo htmlspecialchars($row['BTTV']);?> <br></p1>
        			<p1>Cost: <?php echo htmlspecialchars($row['Cost']);?> <br></p1>
        			<p1>Category:<?php echo htmlspecialchars($row['Category']);?> </p1>
        		</div>
    			
    		</div>
			
		</div>
		
		<div id="descDiv2">
			<div id="weather">
				<div class="tab">
                  <button class="tablinks" onclick="openSeason(event, 'Summer')">Summer</button>
                  <button class="tablinks" onclick="openSeason(event, 'Winter')">Winter</button>
                  <button class="tablinks" onclick="openSeason(event, 'Monsoon')">Monsoon</button>
                </div>
                
                <div id="Summer" class="tabcontent">
                	<div id = "temp">
                		<h3>Min</h3>
                  		<p><?php echo $rowSum['Min'].'°C';?></p>
                	</div>
                  	<div id = "temp">
                		<h3>Max</h3>
                  		<p><?php echo $rowSum['Max'].'°C';?></p>
                	</div>
                	<div id = "temp">
                		<h3>Avg</h3>
                  		<p><?php echo $rowSum['Avg'].'°C';?></p>
                	</div>
                </div>
                
                <div id="Winter" class="tabcontent">
                  <div id = "temp">
                		<h3>Min</h3>
                  		<p><?php echo $rowWin['Min'].'°C';?></p>
                	</div>
                  	<div id = "temp">
                		<h3>Max</h3>
                  		<p><?php echo $rowWin['Max'].'°C';?></p>
                	</div>
                	<div id = "temp">
                		<h3>Avg</h3>
                  		<p><?php echo $rowWin['Avg'].'°C';?></p>
                	</div>
                </div>
                
                <div id="Monsoon" class="tabcontent">
                  <div id = "temp">
                		<h3>Min</h3>
                  		<p><?php echo $rowMon['Min'].'°C';?></p>
                	</div>
                  	<div id = "temp">
                		<h3>Max</h3>
                  		<p><?php echo $rowMon['Max'].'°C';?></p>
                	</div>
                	<div id = "temp">
                		<h3>Avg</h3>
                  		<p><?php echo $rowMon['Avg'].'°C';?></p>
                	</div>
                </div>
               
            </div>
             <div style="float:left" id="directions-panel"></div>
             <INPUT type="button" style="height:auto" value="Route" class="button" onclick="getRoute()">
			<div id="map" style="width:100%;height:100%"></div>
		</div>
		<div class="group" id="group1">
	<div class="intRecc">
 		<label id="p2">Recommendations</label>
 		<div id = "inter">
 			<!-- each div inside this has the class "recc" -->

 		</div>
 	</div>
 	<div class = "hisRecc">
	
 		<div id = "history">
 			<!-- each div inside this has the class "recc" -->
 		</div>
 	</div>
	</div>
	</div>
	</body>
	<script>
	function openSeason(evt, seasonName) {
	    // Declare all variables
	    var i, tabcontent, tablinks;

	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }

	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the button that opened the tab
	    document.getElementById(seasonName).style.display = "block";
	    evt.currentTarget.className += " active";
	}
	</script>
	
	<script src = "getReccTags.js"></script>
	<script src = "getReccHistory.js"></script>
	<script id = "mapScript"  src = "googleMapsGeocode.js">	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrdXOeacO4vMg8PaDu5iLIsQzwrnuW8R8&callback=myMap"></script>
	<script src = "weatherTabs.js"></script>
</html>