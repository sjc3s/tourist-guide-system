
CREATE TABLE IF NOT EXISTS `Tags` (
  `TID` INT NOT NULL AUTO_INCREMENT,
  `Temple` INT NOT NULL,
  `Pilgrimage` INT NOT NULL,
  `Waterfall` INT NOT NULL,
  `WIld_Life` INT NOT NULL,
  `Historical` INT NOT NULL,
  `Nature` INT NOT NULL,
  `Hill_Station` INT NOT NULL,
  `Beach` INT NOT NULL,
  `Architecture` INT NOT NULL,
  PRIMARY KEY (`TID`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `Guide`.`Monsoon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Monsoon` (
  `MID` INT NOT NULL AUTO_INCREMENT,
  `Min` FLOAT(5) NULL,
  `Avg` FLOAT(5) NULL,
  `Max` FLOAT(5) NULL,
  PRIMARY KEY (`MID`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `Guide`.`Winter`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Winter` (
  `WID` INT NOT NULL AUTO_INCREMENT,
  `Min` FLOAT(5) NULL,
  `Avg` FLOAT(5) NULL,
  `Max` FLOAT(5) NULL,
  PRIMARY KEY (`WID`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `Guide`.`Summer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Summer` (
  `SID` INT NOT NULL AUTO_INCREMENT,
  `Min` FLOAT(5) NULL,
  `Avg` FLOAT(5) NULL,
  `Max` FLOAT(5) NULL,
  PRIMARY KEY (`SID`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `Guide`.`State`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `State` (
  `PID` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NULL,
  `BTTV` VARCHAR(45) NULL,
  `Cost` INT NULL,
  `Category` VARCHAR(45) NULL,
  `Tags_TID` INT NOT NULL,
  `Monsoon_MID` INT NOT NULL,
  `Winter_WID` INT NOT NULL,
  `Summer_SID` INT NOT NULL,
  `Img_src` VARCHAR(45) NULL,
  PRIMARY KEY (`PID`),
  INDEX `fk_State_Tags_idx` (`Tags_TID` ASC),
  INDEX `fk_State_Monsoon1_idx` (`Monsoon_MID` ASC),
  INDEX `fk_State_Winter1_idx` (`Winter_WID` ASC),
  INDEX `fk_State_Summer1_idx` (`Summer_SID` ASC),
  CONSTRAINT `fk_State_Tags`
    FOREIGN KEY (`Tags_TID`)
    REFERENCES `Tags` (`TID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_State_Monsoon1`
    FOREIGN KEY (`Monsoon_MID`)
    REFERENCES `Monsoon` (`MID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_State_Winter1`
    FOREIGN KEY (`Winter_WID`)
    REFERENCES `Winter` (`WID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_State_Summer1`
    FOREIGN KEY (`Summer_SID`)
    REFERENCES `Summer` (`SID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

